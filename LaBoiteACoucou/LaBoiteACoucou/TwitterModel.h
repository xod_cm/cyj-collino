//
//  TwitterModel.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterModel : NSObject
{
    NSString *msg;
    UIImage *img;
    NSString *date;
}

- (id) initWithDictionary: (NSDictionary *) dic;

@property (nonatomic) NSString *msg;
@property (nonatomic) NSString *date;
@property (nonatomic) UIImage *img;

@end
