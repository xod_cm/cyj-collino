//
//  TwitterCell.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TwitterModel;

@interface TwitterCell : UITableViewCell


@property (nonatomic, retain) IBOutlet UIImageView *imageV;
@property (nonatomic, retain) IBOutlet UILabel *text;
@property (nonatomic, retain) IBOutlet UILabel *date;

- (void) setTwitter: (TwitterModel *) twitter;

@end
