//
//  FacebookModel.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "FacebookModel.h"

@implementation FacebookModel

@synthesize content = _content;

- (id) initWithDictionary: (NSDictionary *) dictionary;
{
    if (self = [super init])
    {
        _content = [dictionary objectForKey: @"content"];
    }
    return (self);
}

@end
