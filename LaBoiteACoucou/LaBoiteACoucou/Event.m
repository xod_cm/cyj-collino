//
//  Event.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "Event.h"
#import "Jours.h"


@implementation Event

@dynamic id;
@dynamic horaire;
@dynamic isActivated;
@dynamic sonnerie;
@dynamic jours;

@end
