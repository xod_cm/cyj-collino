//
//  RootMainViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 22/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "RootMainViewController.h"
#import "SocialViewController.h"
#import "VideosViewController.h"
#import "PhotosViewController.h"
#import "EventsViewController.h"
#import "Optic2000Pool.h"


@interface RootMainViewController ()

@end

@implementation RootMainViewController

@synthesize imageView = _imageView, day = _day, clock = _clock, tableView = _tableView, subView = _subView, left = _left, right = _right;

- (void) clockReset
{    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    [_clock setText:date];
}

- (void)setupRecognizers
{
    _left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideMenu:)];
    _left.direction = UISwipeGestureRecognizerDirectionLeft;
    _left.delegate = self; // Very important
    [self.view addGestureRecognizer:_left];
    
    _right = [[UISwipeGestureRecognizer alloc] initWithTarget:self     action:@selector(showMenu:)];
    _right.delegate = self; // Very important
    _right.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:_right];
}

- (void) toReveils
{
    EventsViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"eventsViewController"];
    [[self navigationController] pushViewController: rootView animated: YES];

}

- (void) toPhotos
{
    PhotosViewController *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"photosViewController"];
    [[self navigationController] pushViewController: rootView animated: YES];

}

- (void) toMusiques
{
    VideosViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"videosViewController"];
    [[self navigationController] pushViewController: rootView animated: YES];

}

- (void) toResSoc
{
    SocialViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"socialViewController"];
    [[self navigationController] pushViewController: rootView animated: YES];

}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden: YES];
    [super viewWillAppear: animated];
    isMenuShowed = NO;
}

- (void) changeBackground
{
    NSArray *imageArray = [[Optic2000Pool sharedPointer] arrayOfPictures];
    
    int i = rand() % [imageArray count];
    NSString *file = [NSString stringWithFormat: @"%@.jpg", [imageArray objectAtIndex: i]];
    [_imageView setImage: [UIImage imageNamed: file]];
}


- (void)viewDidLoad
{
    [self setupRecognizers];
    isMenuShowed = NO;
    arrayOfItems = @[@"Reveils", @"Photos", @"Musiques", @"Reseaux Sociaux"];
    arrayOfMethods = @[@"toReveils", @"toPhotos", @"toMusiques", @"toResSoc"];
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
      
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *frLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_FR"];
    [formatter setLocale:frLocale];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *date = [formatter stringFromDate:[NSDate date]];
    [_day setText:date];
    
    
    [NSTimer scheduledTimerWithTimeInterval: 1 target: self selector:@selector(clockReset) userInfo:nil repeats:TRUE];
    [NSTimer scheduledTimerWithTimeInterval: 30 target: self selector:@selector(changeBackground) userInfo:nil repeats:TRUE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(RootFlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"])
    {
        [[segue destinationViewController] setDelegate:self];
    }
}

- (void) _showMenu
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelay:0.3];
    [UIView setAnimationCurve: UIViewAnimationCurveLinear];
    [_tableView setHidden: NO];
    [_tableView setUserInteractionEnabled: YES];
    [_tableView setScrollEnabled: YES];
    [_tableView setMultipleTouchEnabled: YES];
    [_tableView setIsAccessibilityElement: YES];
    [_tableView setAllowsSelection: YES];
    
    CGRect bounds = [_tableView frame];
    bounds.origin.x += 171;
    [_tableView setFrame: bounds];
    
    bounds = [_subView frame];
    bounds.origin.x += 171;
    [_subView setFrame: bounds];
    
    [UIView commitAnimations];
    isMenuShowed = YES;
}


- (void) _hideMenu
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelay:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [_tableView setHidden: NO];
    
    
    CGRect bounds = [_tableView frame];
    bounds.origin.x -= 171;
    [_tableView setFrame: bounds];

    bounds = [_subView frame];
    bounds.origin.x -= 171;
    [_subView setFrame: bounds];
    
    
    
    [UIView commitAnimations];
    isMenuShowed = NO;
}


- (IBAction)showMenu: (id) sender
{
    NSLog(@"SHOWMENU");
    if (!isMenuShowed)
        [self _showMenu];
}


- (IBAction)hideMenu: (id) sender
{
    NSLog(@"HIDEMENU");

    if (isMenuShowed)
        [self _hideMenu];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayOfItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *str = [arrayOfItems objectAtIndex: indexPath.row];
    
    [[cell textLabel] setText: str];
    
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [arrayOfMethods objectAtIndex: indexPath.row];
    if ([self respondsToSelector: NSSelectorFromString(str)])
    {
        [self.navigationController.navigationBar setHidden: NO];

        [self performSelector: NSSelectorFromString(str) withObject:nil];
    }
}


@end
