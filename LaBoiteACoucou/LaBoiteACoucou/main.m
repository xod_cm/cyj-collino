//
//  main.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 22/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RootAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RootAppDelegate class]));
    }
}
