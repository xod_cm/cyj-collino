//
//  EventCell.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "EventCell.h"
#import "Event.h"
#import "Jours.h"
#import "Optic2000Pool.h"

@implementation EventCell

@synthesize days = _days, horaire = _horaire, song = _song, isActivated = _isActivated, eevent = _event;

- (IBAction)modifyBooleanValue:(id)sender
{
    NSLog(@"ModifyBooleanValue");
    _event.isActivated = [NSNumber numberWithBool: _isActivated.isOn];
    NSError *error = nil;
    if (![[[Optic2000Pool sharedPointer] managedObjectContext] save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    [[Optic2000Pool sharedPointer] reloadNotifications];

}

- (void) modifyBooleanValue
{
    NSLog(@"ModifyBooleanValue");
    _event.isActivated = [NSNumber numberWithBool: _isActivated.isOn];
    NSError *error = nil;
    if (![[[Optic2000Pool sharedPointer] managedObjectContext] save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [_isActivated addTarget: self action: @selector(modifyBooleanValue) forControlEvents:UIControlEventValueChanged];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSString *) stringForNSSet: (NSSet *) set
{
    int i = 0;
    NSString *str = @"";
    
    for (Jours *jour in set)
    {
        if (i == 0)
            str = [str stringByAppendingFormat: @"%@", [jour.jour substringToIndex: 3]];
        else
            str = [str stringByAppendingFormat: @",%@", [jour.jour substringToIndex: 3]];
            i++;
    }
    
    return str;
}


- (void) setEvent: (Event *) event
{
    _event = event;
    
    [_days setText: [self stringForNSSet: event.jours]];
    
    [_horaire setText: event.horaire];
    [_song setText: event.sonnerie];
    [_isActivated setOn: [event.isActivated boolValue]];
   
}

@end
