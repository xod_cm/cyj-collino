//
//  SocialViewController.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *dataTwitter;
    NSMutableArray *dataFacebook;
}

- (IBAction)changeValue:(id)sender;

@property (nonatomic, retain) IBOutlet UISegmentedControl *segment;
@property (nonatomic, retain) IBOutlet UITableView *tableView;

@end
