//
//  RootFlipsideViewController.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 22/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootFlipsideViewController;

@protocol RootFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(RootFlipsideViewController *)controller;
@end

@interface RootFlipsideViewController : UIViewController

@property (weak, nonatomic) id <RootFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
