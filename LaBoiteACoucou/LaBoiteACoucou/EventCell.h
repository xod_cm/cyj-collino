//
//  EventCell.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@interface EventCell : UITableViewCell
{
    
}

@property (nonatomic, retain) Event *eevent;

- (IBAction)modifyBooleanValue:(id)sender;
- (void) setEvent: (Event *) event;

@property (nonatomic, retain) IBOutlet UILabel *horaire;
@property (nonatomic, retain) IBOutlet UILabel *days;
@property (nonatomic, retain) IBOutlet UILabel *song;
@property (nonatomic, retain) IBOutlet UISwitch *isActivated;

@end
