//
//  VideosCell.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "VideosCell.h"

@implementation VideosCell

@synthesize musique = _musique, button = _button;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (IBAction)pushButton:(id)sender
{
    if (isPlaying)
    {
        isPlaying = NO;
        if (player)
            [player stop];
        [_button setTitle: @">" forState: UIControlStateNormal];
    }
    else
    {
        isPlaying = YES;
        if (!player)
        {
            NSLog(@"_musique: %@", _musique.text);
            
            NSString *soundFilePath = [[NSBundle mainBundle] pathForResource: _musique.text ofType:@"wav"];
            NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
            player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL
                                                            error:nil];
            player.numberOfLoops = -1;
            NSLog(@"Player: %@", player);
        }
        [player play];
        [_button setTitle: @"||" forState: UIControlStateNormal];

    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
