//
//  PhotosViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotosCollectionViewCell.h"
#import "Optic2000Pool.h"
#import "DetailPhotoViewController.h"

@interface PhotosViewController ()

@end

@implementation PhotosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    arrayOfImages = [[Optic2000Pool sharedPointer] arrayOfPictures];
    [super viewDidLoad];
    self.title = @"Photos";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    int i = 0;
    
    i = [arrayOfImages count] % 2;
    
    return (([arrayOfImages count] / 2) + (i ? 1 : 0));
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([arrayOfImages count] < ((section + 1) * 2))
        return ([arrayOfImages count] % 2);
    else if ([arrayOfImages count] != 0)
        return 2;
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: @"photosCell" forIndexPath: indexPath];
  
    [cell setImage: [NSString stringWithFormat: @"%@.jpg", [arrayOfImages objectAtIndex: (indexPath.section * 2) + indexPath.row]]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetailPhotoViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"detailPhotoViewController"];
    [rootView setImageName: [NSString stringWithFormat: @"%@.jpg", [arrayOfImages objectAtIndex: (indexPath.section * 2) + indexPath.row]]];
    [[self navigationController] pushViewController: rootView animated: YES];
}

@end
