//
//  SocialViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "SocialViewController.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"
#import "TwitterModel.h"
#import "TwitterCell.h"
#import "FacebookCell.h"
#import "FacebookModel.h"

@interface SocialViewController ()

@end

@implementation SocialViewController

@synthesize tableView = _tableView, segment = _segment;

- (IBAction) changeValue:(id)sender
{
    [_tableView reloadData];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadTwitterFeeds
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://api.twitter.com/1/statuses/user_timeline.json?screen_name=JohnnySjh"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSArray *arrayOfTweets = (NSArray *) JSON;
        
        for (NSDictionary *tweet in arrayOfTweets)
        {
            TwitterModel *model = [[TwitterModel alloc] initWithDictionary: tweet];
            [dataTwitter addObject: model];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
     {
    }];
    
    [operation start];

}

- (void) loadFacebookFeeds
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.facebook.com/feeds/page.php?format=json&id=366036613472952"]];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        NSDictionary *dictOfFb = (NSDictionary *) JSON;
        
        NSArray *arrayOfFB = [dictOfFb objectForKey: @"entries"];
        
       for (NSDictionary *feeds in arrayOfFB)
        {
            FacebookModel *model = [[FacebookModel alloc] initWithDictionary: feeds];
            [dataFacebook addObject: model];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                         }];
    
    [operation start];
}

- (void)viewDidLoad
{
    dataFacebook = [[NSMutableArray alloc] init];
    dataTwitter  = [[NSMutableArray alloc] init];
    [super viewDidLoad];
    
    [self loadTwitterFeeds];
    [self loadFacebookFeeds];
    
    self.title = @"Social Coucou";
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([_segment selectedSegmentIndex] == 0)
        return [dataFacebook count];
    else
        return [dataTwitter count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *TwitterIdentifier = @"TCell";
    static NSString *FacebookIdentifier = @"FCell";
    if ([_segment selectedSegmentIndex] == 0)
    {
        FacebookCell *cell = [tableView dequeueReusableCellWithIdentifier: FacebookIdentifier  forIndexPath:indexPath];
        [cell setFacebook: [dataFacebook objectAtIndex: indexPath.row]];
        
        return cell;
    }
    else
    {
        TwitterCell *cell = [tableView dequeueReusableCellWithIdentifier:TwitterIdentifier forIndexPath:indexPath];
        
        [cell setTwitter: [dataTwitter objectAtIndex: indexPath.row]];
        
        return cell;
    }
    
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_segment.selectedSegmentIndex == 1)
        return 127;
    return 200;
}
@end
