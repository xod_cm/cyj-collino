//
//  RootFlipsideViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 22/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "RootFlipsideViewController.h"

@interface RootFlipsideViewController ()

@end

@implementation RootFlipsideViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate flipsideViewControllerDidFinish:self];
}

@end
