//
//  AddEventViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "AddEventViewController.h"
#import "DaysViewController.h"
#import "SonnerieViewController.h"
#import "Event.h"
#import "Optic2000Pool.h"

@interface AddEventViewController ()

@end

@implementation AddEventViewController

@synthesize tableView = _tableView, datePicker = _datePicker;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) toRecurrence
{
    DaysViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"daysViewController"];
    
    [rootView setEvent: event];

    [[self navigationController] pushViewController: rootView animated: YES];
}

- (void) toSonnerie
{
    SonnerieViewController  *rootView = [[self storyboard] instantiateViewControllerWithIdentifier:@"sonnerieViewController"];
    
    [rootView setEvent: event];
    
    [[self navigationController] pushViewController: rootView animated: YES];
}

- (void) addEvent
{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *horaire = [dateformatter stringFromDate:[_datePicker date]];
    
    event.horaire = horaire;
    event.isActivated = [NSNumber numberWithBool: YES];
    NSError *error = nil;
    if (![[[Optic2000Pool sharedPointer] managedObjectContext] save:&error]) {
        NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    [[Optic2000Pool sharedPointer] reloadNotifications];
    
    [self.navigationController popViewControllerAnimated: YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
}

- (void)viewDidLoad
{
    link = @[@"Recurrence", @"Sonnerie"];
    linkMethods = @[@"toRecurrence", @"toSonnerie"];
    
    NSManagedObjectContext *context = [[Optic2000Pool sharedPointer] managedObjectContext];
    
    event = [NSEntityDescription insertNewObjectForEntityForName: @"Event" inManagedObjectContext: context];
    
    [super viewDidLoad];
    
    UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAdd target: self action: @selector(addEvent)];
    
    
    [self.navigationItem setRightBarButtonItem: menu];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [link count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *str = [link objectAtIndex: indexPath.row];
    
    [[cell textLabel] setText: str];
    
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [linkMethods objectAtIndex: indexPath.row];
    if ([self respondsToSelector: NSSelectorFromString(str)])
    {
        [self performSelector: NSSelectorFromString(str) withObject:nil];
    }
}

@end
