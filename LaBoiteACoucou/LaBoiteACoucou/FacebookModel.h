//
//  FacebookModel.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FacebookModel : NSObject
{
    NSString *content;
}

- (id) initWithDictionary: (NSDictionary *) dictionary;

@property (nonatomic) NSString *content;

@end
