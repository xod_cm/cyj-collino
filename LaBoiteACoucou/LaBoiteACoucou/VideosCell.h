//
//  VideosCell.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideosCell : UITableViewCell
{
    BOOL isPlaying;
    AVAudioPlayer *player;
}
@property (nonatomic, retain) IBOutlet UILabel *musique;
@property (nonatomic, retain) IBOutlet UIButton *button;
- (IBAction)pushButton:(id)sender;
@end
