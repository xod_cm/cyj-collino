//
//  Optic2000Pool.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Optic2000Pool : NSObject
{
    NSArray *arrayOfMusic;
    NSArray *arrayOfMusicNotif;
    NSArray *arrayOfPictures;
}

- (id) init;
+ (Optic2000Pool *) sharedPointer;

- (void) reloadNotifications;

@property (retain, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (retain, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (retain, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSArray *arrayOfMusic;
@property (nonatomic, strong) NSArray *arrayOfMusicNotif;

@property (nonatomic, strong) NSArray *arrayOfPictures;

@end
