//
//  TwitterCell.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "TwitterCell.h"
#import "TwitterModel.h"

@implementation TwitterCell

@synthesize imageV = _imageV, date = _date, text = _text;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setTwitter:(TwitterModel *)twitter
{
    [_text setText: [twitter msg]];
    [_date setText: [twitter date]];
    [_imageV setImage: [twitter img]];
}

@end
