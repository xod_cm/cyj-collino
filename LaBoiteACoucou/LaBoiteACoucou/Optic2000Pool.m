//
//  Optic2000Pool.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "Optic2000Pool.h"
#import "Event.h"
#import "Jours.h"

static Optic2000Pool *shared = nil;

@implementation Optic2000Pool
@synthesize managedObjectContext = _managedObjectContext, managedObjectModel = _managedObjectModel, persistentStoreCoordinator = _persistentStoreCoordinator, arrayOfMusic = _arrayOfMusic, arrayOfPictures = _arrayOfPictures, arrayOfMusicNotif = _arrayOfMusicNotif;

- (id) init
{
    if (self = [super init])
    {
        _arrayOfMusicNotif = @[@"l_attente_notif.wav"];
        _arrayOfMusic = @[@"l_attente"];
        _arrayOfPictures = @[@"johnny", @"johnny2", @"johnny3", @"johnny4", @"large", @"old"];
    }
    
    return self;
}

+ (Optic2000Pool *) sharedPointer
{
    if (!shared)
        shared = [[Optic2000Pool alloc] init];
    
    return shared;
}

- (void) notificationForEvent: (Event *) event
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *now = [NSDate date];
    
    // set components for time 7:00 a.m. Monday
    NSDateComponents *componentsForFireDate = [calendar components:(NSYearCalendarUnit |   NSHourCalendarUnit | NSMinuteCalendarUnit| NSSecondCalendarUnit | NSWeekdayCalendarUnit) fromDate: now];
    
    NSArray *horaire = [event.horaire componentsSeparatedByString:@":"];
    
    NSDictionary *dicWeekDay = @{@"lundi": @0, @"mardi": @1, @"mercredi":@2, @"jeudi":@3, @"vendredi":@4, @"samedi": @5, @"dimanche": @6};
    
    NSLog(@"Event: %@", event);
    
   /* for (Jours *jour in event.jours)
    {
        NSNumber *numb = [dicWeekDay objectForKey: jour.jour];
        
        [componentsForFireDate setWeekday: [numb integerValue]] ;
        [componentsForFireDate setHour: [[horaire objectAtIndex:0] integerValue]] ;
        [componentsForFireDate setMinute:[[horaire objectAtIndex:1] integerValue] + 1] ;
        [componentsForFireDate setSecond:0] ;
    
        NSDate *fireDateOfNotification = [calendar dateFromComponents: componentsForFireDate];
    
        // Create the notification
        UILocalNotification *notification = [[UILocalNotification alloc]  init] ;
    
        notification.fireDate = nil ;
        notification.timeZone = [NSTimeZone localTimeZone] ;
        notification.alertBody = [NSString stringWithFormat: @"Ah que Johnny te reveille!"] ;
    
        notification.repeatInterval= NSWeekCalendarUnit;
        if (event.sonnerie)
            notification.soundName = event.sonnerie;
    
        NSLog(@"Local Notification squeduled: %@", notification);
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification] ;
    }
    */
    
    UILocalNotification *notification = [[UILocalNotification alloc]  init] ;
    
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
    notification.timeZone = [NSTimeZone localTimeZone] ;
    notification.alertBody = [NSString stringWithFormat: @"Ah que Johnny te reveille!"] ;
    
  //  notification.repeatInterval= NSWeekCalendarUnit;
    if (event.sonnerie)
        notification.soundName = event.sonnerie;
    else
        notification.soundName = UILocalNotificationDefaultSoundName;

    NSLog(@"Local Notification squeduled: %@", notification);
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification] ;
    
}


- (void) reloadNotifications
{
    NSManagedObjectContext *context = [[Optic2000Pool sharedPointer] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event"
                                              inManagedObjectContext: context];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchResults = [context
                             executeFetchRequest:fetchRequest
                             error:&error];
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    for (Event *event in fetchResults)
    {
        if ([event.isActivated boolValue])
            [self notificationForEvent: event];
    }
}

@end
