//
//  PhotosCollectionViewCell.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell
{
    BOOL isLoaded;
}
- (void) setImage: (NSString *) url;
- (BOOL) hasBeenLoaded;

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@end
