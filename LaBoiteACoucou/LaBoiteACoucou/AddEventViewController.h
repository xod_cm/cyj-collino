//
//  AddEventViewController.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@interface AddEventViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *link;
    NSArray *linkMethods;
    Event *event;
}
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) IBOutlet UITableView *tableView;


@end
