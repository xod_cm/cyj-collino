//
//  TwitterModel.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "TwitterModel.h"

@implementation TwitterModel

@synthesize date = _date, img = _img, msg = _msg;

- (id) initWithDictionary: (NSDictionary *) dictionary
{
    if (self = [super init])
    {
    NSDictionary *user = [dictionary objectForKey: @"user"];
    _date = [[NSString alloc] initWithString: [user objectForKey: @"created_at"]];
    _msg = [[NSString alloc] initWithString: [dictionary objectForKey: @"text"]];
    _img = nil;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: [user objectForKey: @"profile_image_url"]]]];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (image)
            {
                _img = image;
            }
        });
    });
    }
    
    return self;
}

@end
