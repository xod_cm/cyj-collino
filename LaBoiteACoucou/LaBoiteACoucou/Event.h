//
//  Event.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Jours;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * horaire;
@property (nonatomic, retain) NSNumber * isActivated;
@property (nonatomic, retain) NSString * sonnerie;
@property (nonatomic, retain) NSSet *jours;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addJoursObject:(Jours *)value;
- (void)removeJoursObject:(Jours *)value;
- (void)addJours:(NSSet *)values;
- (void)removeJours:(NSSet *)values;

@end
