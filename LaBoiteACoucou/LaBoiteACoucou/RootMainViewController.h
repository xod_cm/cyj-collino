//
//  RootMainViewController.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 22/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "RootFlipsideViewController.h"

#import <CoreData/CoreData.h>

@interface RootMainViewController : UIViewController <RootFlipsideViewControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>
{
    BOOL isMenuShowed;
    NSArray *arrayOfItems;
    NSArray *arrayOfMethods;
}
@property (nonatomic, retain) IBOutlet  UILabel *clock;
@property (nonatomic, retain) IBOutlet  UILabel *day;
@property (nonatomic, retain) IBOutlet  UIImageView *imageView;
@property (nonatomic, retain) IBOutlet  UIView *subView;
@property (nonatomic, retain) IBOutlet  UITableView *tableView;

@property (nonatomic)  UISwipeGestureRecognizer *left;
@property (nonatomic)  UISwipeGestureRecognizer *right;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)showMenu: (id) sender;
- (IBAction)hideMenu: (id) sender;

@end
