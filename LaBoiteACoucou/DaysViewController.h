//
//  DaysViewController.h
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;
@interface DaysViewController : UITableViewController
{
    NSArray *jours;
}
@property (nonatomic, retain) Event *event;
@end
