//
//  DaysViewController.m
//  LaBoiteACoucou
//
//  Created by Morgan Collino on 23/03/13.
//  Copyright (c) 2013 Morgan Collino. All rights reserved.
//

#import "DaysViewController.h"
#import "Event.h"
#import "Jours.h"
#import "Optic2000Pool.h"

@interface DaysViewController ()

@end

@implementation DaysViewController

@synthesize event = _event;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    jours = @[@"lundi",@"mardi",@"mercredi", @"jeudi",@"vendredi", @"samedi",@"dimanche"];
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [jours count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    for (Jours *jour in _event.jours)
    {
        NSLog(@"JOUR: %@", jour.jour);
        if ([jour.jour isEqualToString: [jours objectAtIndex: indexPath.row]])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            break;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }

    [[cell textLabel] setText: [jours objectAtIndex: indexPath.row]];
    
    return cell;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
    
    if (!(cell.accessoryType == UITableViewCellAccessoryCheckmark))
    {
        NSManagedObjectContext *context = [[Optic2000Pool sharedPointer] managedObjectContext];
    
        Jours *jour = [NSEntityDescription insertNewObjectForEntityForName: @"Jours" inManagedObjectContext: context];
    
        jour.jour = [jours objectAtIndex: indexPath.row];
        
        [_event addJoursObject: jour];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {        
        Jours *jour = nil;
        for (Jours *_jour in _event.jours)
        {
            if ([_jour.jour isEqualToString: [jours objectAtIndex: indexPath.row]])
            {
                jour = _jour;
                break;
            }
        }
        
        if (jour)
        {
            [_event removeJoursObject: jour];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}

@end
